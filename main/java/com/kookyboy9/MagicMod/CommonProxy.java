package com.kookyboy9.MagicMod;

import com.kookyboy9.MagicMod.Events.PlayerEvents;
import com.kookyboy9.MagicMod.GUI.GUIHandler;
import com.kookyboy9.MagicMod.Items.ModItems;
import com.kookyboy9.MagicMod.Magic.SpellRegisterer;
import com.kookyboy9.MagicMod.MagicCapability.CapabilityHandler;
import com.kookyboy9.MagicMod.MagicCapability.IMagicCapability;
import com.kookyboy9.MagicMod.MagicCapability.MagicCapability;
import com.kookyboy9.MagicMod.MagicCapability.MagicStorage;
import com.kookyboy9.MagicMod.Packets.PacketDispatcher;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
//Only do things you want done on both sides in this class.
public class CommonProxy {
	/**
	 * Returns a side-appropriate EntityPlayer for use during message handling
	 */
	public EntityPlayer getPlayerEntity(MessageContext ctx) {
	 return ctx.getServerHandler().playerEntity;
	}
    public void preInit(FMLPreInitializationEvent e) {
    	ModItems.createItems();
    }

    public void init(FMLInitializationEvent e) {

        CapabilityManager.INSTANCE.register(IMagicCapability.class, new MagicStorage(), MagicCapability.class);
        PacketDispatcher.registerPackets();
        MinecraftForge.EVENT_BUS.register(new CapabilityHandler());
        MinecraftForge.EVENT_BUS.register(new PlayerEvents());
    	SpellRegisterer.init();
    }
    
    public void postInit(FMLPostInitializationEvent e) {
    	NetworkRegistry.INSTANCE.registerGuiHandler(MagicMod.instance, new GUIHandler());
    }
}
