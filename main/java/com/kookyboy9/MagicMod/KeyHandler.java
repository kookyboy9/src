package com.kookyboy9.MagicMod;

import org.lwjgl.input.Keyboard;

import com.kookyboy9.MagicMod.GUI.GUIHandler;

import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.settings.KeyBinding;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.InputEvent.KeyInputEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class KeyHandler {
	public static KeyBinding MAGIC_GUI_BUTTON = new KeyBinding("Brings Up magic GUI", Keyboard.KEY_M, "Magic");
	
	public static void init(){
		ClientRegistry.registerKeyBinding(MAGIC_GUI_BUTTON);
	}
	@SideOnly(Side.CLIENT)
	@SubscribeEvent
	public void KeyEvents(KeyInputEvent e){
		if(MAGIC_GUI_BUTTON.isPressed()){
			EntityPlayerSP playerIn = Minecraft.getMinecraft().thePlayer;
			playerIn.openGui(MagicMod.instance, GUIHandler.MOD_MAGIC_GUI, Minecraft.getMinecraft().thePlayer.worldObj, (int) playerIn.posX, (int) playerIn.posY, (int) playerIn.posZ);
		}
	}
	
}
