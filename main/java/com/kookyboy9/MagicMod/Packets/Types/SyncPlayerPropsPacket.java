package com.kookyboy9.MagicMod.Packets.Types;

import com.kookyboy9.MagicMod.MagicMod;
import com.kookyboy9.MagicMod.MagicCapability.MagicProvider;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;



/**
* If you are going with the more simple route, the only difference is your inner handler class signature
* will look like this:
*
* public static class Handler implements IMessageHandler<OpenGuiMessage, IMessage>
*
* and the only method will be:
*
* public IMessage onMessage(OpenGuiMessage, MessageContext)
*
* Note that you can't immediately tell what side you are supposed to be on just from looking at the method,
* and you have to do some work to get the player if you want it. But we fixed all that xD
*/
public class SyncPlayerPropsPacket implements IMessage {
// this will store the id of the gui to open
private NBTTagCompound id;

// The basic, no-argument constructor MUST be included to use the new automated handling
public SyncPlayerPropsPacket() {}

// if there are any class fields, be sure to provide a constructor that allows
// for them to be initialized, and use that constructor when sending the packet
public SyncPlayerPropsPacket(EntityPlayer player) {
	this.id = player.getCapability(MagicProvider.MAGIC_CAP, null).WriteToNBT();
}

@Override
public void fromBytes(ByteBuf buffer) {
// basic Input/Output operations, very much like DataInputStream
id = ByteBufUtils.readTag(buffer);
}

@Override
public void toBytes(ByteBuf buffer) {
// basic Input/Output operations, very much like DataOutputStream
ByteBufUtils.writeTag(buffer, id);

}

public static class Handler extends AbstractClientMessageHandler<SyncPlayerPropsPacket> {
@Override
public IMessage handleClientMessage(EntityPlayer player, SyncPlayerPropsPacket message, 
MessageContext ctx) {
// because we sent the gui's id with the packet, we can handle all cases with one line:
	if(player == null){
		player = Minecraft.getMinecraft().thePlayer;
	}
	player.getCapability(MagicProvider.MAGIC_CAP, null).ReadFromNBT(message.id);
return null;
}
}
}