package com.kookyboy9.MagicMod.Packets.Types;

import com.kookyboy9.MagicMod.MagicMod;
import com.kookyboy9.MagicMod.MagicCapability.MagicProvider;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;



/**
* If you are going with the more simple route, the only difference is your inner handler class signature
* will look like this:
*
* public static class Handler implements IMessageHandler<OpenGuiMessage, IMessage>
*
* and the only method will be:
*
* public IMessage onMessage(OpenGuiMessage, MessageContext)
*
* Note that you can't immediately tell what side you are supposed to be on just from looking at the method,
* and you have to do some work to get the player if you want it. But we fixed all that xD
*/
public class SyncManaPacket implements IMessage {
// this will store the id of the gui to open
private float id;

// The basic, no-argument constructor MUST be included to use the new automated handling
public SyncManaPacket() {}

// if there are any class fields, be sure to provide a constructor that allows
// for them to be initialized, and use that constructor when sending the packet
public SyncManaPacket(float id) {
this.id = id;
}

@Override
public void fromBytes(ByteBuf buffer) {
// basic Input/Output operations, very much like DataInputStream
id = buffer.readFloat();
}

@Override
public void toBytes(ByteBuf buffer) {
// basic Input/Output operations, very much like DataOutputStream
buffer.writeFloat(id);
}

public static class Handler extends AbstractClientMessageHandler<SyncManaPacket> {
@Override
public IMessage handleClientMessage(EntityPlayer player, SyncManaPacket message, 
MessageContext ctx) {
// because we sent the gui's id with the packet, we can handle all cases with one line:
	player.getCapability(MagicProvider.MAGIC_CAP, null).setMana(message.id);
return null;
}
}
}