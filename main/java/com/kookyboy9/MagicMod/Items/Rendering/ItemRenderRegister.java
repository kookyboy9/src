package com.kookyboy9.MagicMod.Items.Rendering;

import com.kookyboy9.MagicMod.MagicMod;
import com.kookyboy9.MagicMod.Items.ModItems;

import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.item.Item;

public final class ItemRenderRegister {
	public static String modid = "magicmod";
	
    public static void registerItemRenderer() {
    	//when you add an item, call this using the instance of it from the ModItems class.
    	//when you do this, make sure that there is a .json file named (localized name).json that contains 
    	//the location of the .png for the item, the rotation info for the item, and the scaling info for the item.
    	reg(ModItems.tutorialItem);
    	reg(ModItems.guideItem);
    	reg(ModItems.keyStone);
    }
    public static void reg(Item item) {
        Minecraft.getMinecraft().getRenderItem().getItemModelMesher()
        .register(item, 0, new ModelResourceLocation(modid + ":" + item.getUnlocalizedName().substring(5), "inventory"));
        MagicMod.log.info("Substring is: ["+item.getUnlocalizedName().substring(5)+"]");
    }
}
