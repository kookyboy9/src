package com.kookyboy9.MagicMod.Items;

import com.kookyboy9.MagicMod.Items.Types.GuideItem;
import com.kookyboy9.MagicMod.Items.Types.MagicKeystone;
import com.kookyboy9.MagicMod.Items.Types.ModItem;

import net.minecraft.item.Item;
import net.minecraftforge.fml.common.registry.GameRegistry;

public final class ModItems {
	//Declare item instances here. 
	public static Item tutorialItem;
	public static Item guideItem;
	public static Item keyStone;
    public static void createItems() {
    	//Initialize and add items here. 
    	//format is GameRegistry.registerItem((item object from above) = new (class extending item)(any arguments that class needs), (localized name));
    	//When you add an item, don't forget to call the register method in ItemRenderRegister.java.
    	GameRegistry.registerItem(tutorialItem = new ModItem("tutorial_item"), "tutorial_item");
    	GameRegistry.registerItem(guideItem = new GuideItem("guide_item"), "guide_item");
    	GameRegistry.registerItem(keyStone = new MagicKeystone("keystone"), "keystone");
    }
}