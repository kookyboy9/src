package com.kookyboy9.MagicMod.Items.Types;

import com.kookyboy9.MagicMod.MagicMod;
import com.kookyboy9.MagicMod.GUI.GUIHandler;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class GuideItem extends Item {

    public GuideItem() {
        super();
    }
    public GuideItem(String unlocalizedName) {
        super();

        this.setUnlocalizedName(unlocalizedName);
        this.setCreativeTab(CreativeTabs.tabMaterials);
    }
    @Override
    public ItemStack onItemRightClick(ItemStack itemStackIn, World worldIn, EntityPlayer playerIn) {
        if (worldIn.isRemote) {
            playerIn.openGui(MagicMod.instance, GUIHandler.MOD_GUIDE_GUI, worldIn, (int) playerIn.posX, (int) playerIn.posY, (int) playerIn.posZ);
        }
        return itemStackIn;
    }
}
