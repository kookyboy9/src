package com.kookyboy9.MagicMod.Items.Types;

import com.kookyboy9.MagicMod.MagicMod;
import com.kookyboy9.MagicMod.GUI.GUIHandler;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.world.World;

public class MagicKeystone extends Item {
	NBTTagCompound data;
    public MagicKeystone() {
        super();
    }
    public MagicKeystone(String unlocalizedName) {
        super();
        data = new NBTTagCompound();
        this.setUnlocalizedName(unlocalizedName);
        this.setCreativeTab(CreativeTabs.tabMaterials);
    }
    @Override
    public boolean onItemUse(ItemStack stack, EntityPlayer playerIn, World worldIn, BlockPos pos, EnumFacing side, float hitX, float hitY, float hitZ) {
    	data.setInteger("CenterX", (int) pos.getX() );
    	data.setInteger("CenterY", (int) pos.getY() );
    	data.setInteger("CenterZ", (int) pos.getZ() );
    	MagicMod.log.info((int)pos.getX());
    	MagicMod.log.info((int)pos.getY());
    	MagicMod.log.info((int)pos.getZ());
    	return true;
    }
    @Override
    public ItemStack onItemRightClick(ItemStack stack, World world, EntityPlayer player){
		return stack;
    	
    }
}