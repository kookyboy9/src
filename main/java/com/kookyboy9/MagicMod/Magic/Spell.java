package com.kookyboy9.MagicMod.Magic;


import java.util.ArrayList;
import java.util.Iterator;

import com.kookyboy9.MagicMod.Magic.Elements.ElementTypes;


public class Spell
{
	ArrayList<ElementTypes> spell = new ArrayList<ElementTypes>();
	public void addElement(ElementTypes element) 
	{
		System.out.println("Adding "+ element + " to spell");
		(this.spell).add(element);
	}
	public Iterator<ElementTypes> getSpell() 
	{
		return this.spell.iterator();
	}
	public boolean hasElements() 
	{
		return (this.spell.size() > 0);
	}
	public void releaseSpell()
	{
		this.spell.clear();
	}
}	