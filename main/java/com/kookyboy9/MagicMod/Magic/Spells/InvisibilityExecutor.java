package com.kookyboy9.MagicMod.Magic.Spells;

import com.kookyboy9.MagicMod.MagicCapability.IMagicCapability;
import com.kookyboy9.MagicMod.MagicCapability.MagicProvider;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

public class InvisibilityExecutor implements SpellExecutor {

	@Override
	public void executeSpellHand(double x, double y, double z, World world, EntityPlayer player) {
		player.setInvisible(!player.isInvisible());
		IMagicCapability mana = player.getCapability(MagicProvider.MAGIC_CAP, null);
		if(player.isInvisible()){
			mana.addConstantManaDrain(.1f);
		} else {
			mana.removeConstantManaDrain(.1f);
		}
		
	}

	@Override
	public void executeSpellCircle(double radius, double center, World world) {
		// TODO Auto-generated method stub
		
	}

}
