package com.kookyboy9.MagicMod.Magic.Spells;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

public interface SpellExecutor {
	/**
	 * called when you cast from the GUI
	 * @param x World Coordinate
	 * @param y World Coordinate
	 * @param z World Coordinate
	 * @param world World Object
	 * @param player Player Casting
	 */
	public void executeSpellHand(double x, double y, double z, World world, EntityPlayer player);
	/**
	 * called when you cast with a circle in the world.
	 * @param radius
	 * @param center
	 * @param world
	 */
	public void executeSpellCircle(double radius, double center, World world);
}
