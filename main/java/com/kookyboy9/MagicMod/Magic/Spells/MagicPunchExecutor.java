package com.kookyboy9.MagicMod.Magic.Spells;

import com.kookyboy9.MagicMod.MagicMod;
import com.kookyboy9.MagicMod.MagicCapability.IMagicCapability;
import com.kookyboy9.MagicMod.MagicCapability.MagicProvider;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;

public class MagicPunchExecutor implements SpellExecutor {

	@Override
	public void executeSpellHand(double x, double y, double z, World world, EntityPlayer player) {
		IMagicCapability mana = player.getCapability(MagicProvider.MAGIC_CAP, null);
		if(!mana.getMagicPunch()){
			mana.setMagicPunch(true);
		} else {
			mana.setMagicPunch(false);
		}
		
	}

	@Override
	public void executeSpellCircle(double radius, double center, World world) {
		// TODO Auto-generated method stub
		
	}

}
