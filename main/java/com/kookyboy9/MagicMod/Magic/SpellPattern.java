package com.kookyboy9.MagicMod.Magic;


import java.util.ArrayList;
import java.util.Iterator;

import com.kookyboy9.MagicMod.MagicMod;
import com.kookyboy9.MagicMod.Magic.Elements.ElementTypes;
import com.kookyboy9.MagicMod.Magic.Spells.SpellExecutor;
import com.kookyboy9.MagicMod.MagicCapability.IMagicCapability;
import com.kookyboy9.MagicMod.MagicCapability.MagicProvider;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;


public class SpellPattern
{
	private ArrayList<ElementTypes> pattern = new ArrayList<ElementTypes>();
	private SpellExecutor executor;
	private int cost;
	public SpellPattern(SpellExecutor executor,  int cost, ElementTypes... element) 
	{
		for(int c=0;c<element.length;c++) 
		{
			this.pattern.add(element[c]);
		}
		this.executor = executor;
		this.cost = cost;
	}
	public void runExecutorHand(double x, double y, double z, World world, EntityPlayer entity) 
	{
		this.executor.executeSpellHand(x,y,z,world, entity);
	}
	public void runExecutorCircle(double radius, double center, World world) 
	{
		this.executor.executeSpellCircle(radius,center,world);
	}
	public static boolean matchSpell(SpellPattern that, EntityPlayer player) 
	{
		 return SpellPattern.matchSpell(that.pattern.iterator(), player);
	}
	public static  boolean matchSpell(Iterator<ElementTypes> it, EntityPlayer player) 
	{
		Iterator<ElementTypes> itPattern = player.getCapability(MagicProvider.MAGIC_CAP, null).getSpell().getSpell();
		while(it.hasNext() && itPattern.hasNext())
		{
			ElementTypes patternpower = itPattern.next();
			ElementTypes Spellsegment = it.next();
			if(patternpower != Spellsegment) 
			{
				return false;
			}
		}
		if(it.hasNext() || itPattern.hasNext())
		{
			return false;
		}
		MagicMod.log.info("Spell found");
		return true;
	}
	public int getCost() {
		return cost;
		
	}
}
