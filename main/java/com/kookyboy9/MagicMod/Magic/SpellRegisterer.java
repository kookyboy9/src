package com.kookyboy9.MagicMod.Magic;

import java.util.ArrayList;

import com.kookyboy9.MagicMod.Magic.Elements.ElementTypes;
import com.kookyboy9.MagicMod.Magic.Spells.InvisibilityExecutor;
import com.kookyboy9.MagicMod.Magic.Spells.MagicPunchExecutor;

public class SpellRegisterer {
	public static ArrayList<SpellPattern> spells = new ArrayList<SpellPattern>();
	public static void init(){
		spells.add(
				new SpellPattern(
						new InvisibilityExecutor(),
						10,
						ElementTypes.DARK,
						ElementTypes.AIR,
						ElementTypes.LIGHT
						)
				);
		spells.add(new SpellPattern(
						new MagicPunchExecutor(),
						50,
						ElementTypes.EARTH,
						ElementTypes.AIR,
						ElementTypes.FIRE,
						ElementTypes.EARTH
						)
				);
		
	}
}
