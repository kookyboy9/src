package com.kookyboy9.MagicMod.Events;

import java.util.Random;

import com.kookyboy9.MagicMod.MagicMod;
import com.kookyboy9.MagicMod.Magic.SpellPattern;
import com.kookyboy9.MagicMod.Magic.SpellRegisterer;
import com.kookyboy9.MagicMod.MagicCapability.IMagicCapability;
import com.kookyboy9.MagicMod.MagicCapability.MagicProvider;
import com.kookyboy9.MagicMod.Packets.PacketDispatcher;
import com.kookyboy9.MagicMod.Packets.Types.SyncManaPacket;
import com.kookyboy9.MagicMod.Packets.Types.SyncMaxManaPacket;
import com.kookyboy9.MagicMod.Packets.Types.SyncPlayerPropsPacket;

import ibxm.Player;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.world.Explosion;
import net.minecraftforge.event.entity.living.LivingFallEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.entity.player.EntityInteractEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.entity.player.PlayerSleepInBedEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedInEvent;
import net.minecraftforge.fml.common.gameevent.PlayerEvent.PlayerLoggedOutEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent.PlayerTickEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.items.ItemStackHandler;

public class PlayerEvents
{
    @SubscribeEvent
    public void onPlayerLogsIn(PlayerLoggedInEvent event)
    {
        IMagicCapability mana = event.player.getCapability(MagicProvider.MAGIC_CAP, null);
        if(event.player != null && !event.player.worldObj.isRemote){
        	EntityPlayerMP player = (EntityPlayerMP)event.player;
        	if(player != null){
        		PacketDispatcher.sendTo(new SyncManaPacket(mana.getMana()), player);
        		PacketDispatcher.sendTo(new SyncMaxManaPacket(mana.GetMaxMana()), player);
        	} else {
        		MagicMod.log.error("This means something went very wrong.");
        	}
        }
    }


    @SubscribeEvent
    public void onPlayerFalls(LivingFallEvent event)
    {
        Entity entity = event.entity;

        if (!entity.worldObj.isRemote || !(entity instanceof EntityPlayerMP) || event.distance < 3) return;

        EntityPlayer player = (EntityPlayer) entity;
        IMagicCapability mana = player.getCapability(MagicProvider.MAGIC_CAP, null);
        
        float points = mana.getMana();
        float cost = event.distance * 3;

        if (points > cost)
        {
            mana.consumeMana(cost);

            event.setCanceled(true);
        }
    }
    @SubscribeEvent
    public void onAttackEnemy(LivingHurtEvent e){
		if(!e.entity.worldObj.isRemote && e.source.getEntity() != null && e.source.getEntity() instanceof EntityPlayer){
			MagicMod.log.info("Player did damage");
			EntityPlayer player = (EntityPlayer)e.source.getEntity();
			IMagicCapability mana = player.getCapability(MagicProvider.MAGIC_CAP, null);
			if(mana.getMagicPunch()){
				MagicMod.log.info("Magic Punch");
				e.ammount += 3.0f;
				mana.setMagicPunch(false);
			}
	        
		}
	}
    @SubscribeEvent
    public void OnPlayerTick(PlayerTickEvent e){
    	if(e.side == Side.SERVER) {
    		IMagicCapability mana = e.player.getCapability(MagicProvider.MAGIC_CAP, null);
    		if(mana.getConstantManaDrain() > 0.0){
    			mana.consumeMana(mana.getConstantManaDrain());
    			if(mana.getMana() <= 0){
    				if(e.player.isInvisible()){
    					e.player.setInvisible(false);
    					mana.removeConstantManaDrain(.1f);
    				}
    			}
    			if(!mana.getCast()){
    				PacketDispatcher.sendTo(new SyncManaPacket(mana.getMana()), (EntityPlayerMP)e.player);
    			}
    			
    		}
    		if(e.player.isPlayerSleeping()){
    			if( mana.getMana() <= mana.GetMaxMana()/10 && mana.shouldIncreaseMaxMana())
    			{
    				mana.SetMaxMana(mana.GetMaxMana() + (mana.GetMaxMana() / 10));
    				PacketDispatcher.sendTo(new SyncMaxManaPacket(mana.GetMaxMana()), (EntityPlayerMP)e.player);
    				mana.toggleIncreaseMaxMana();
    				MagicMod.log.info("Adding Max Mana in the amount: ");
    				MagicMod.log.info((mana.GetMaxMana() / 10));
    			} else if(mana.getMana() >= mana.GetMaxMana()/10 && !mana.shouldIncreaseMaxMana()) {
    				mana.toggleIncreaseMaxMana();
    			}
    			if(mana.getMana() < mana.GetMaxMana())
    			{
    				mana.fillMana(mana.GetMaxMana() / 1000);
    				PacketDispatcher.sendTo(new SyncManaPacket(mana.getMana()), (EntityPlayerMP)e.player);
    			}
    		}
    		if(mana.getCast()) {
    			MagicMod.log.info("Starting spell matching");
	    		for(int i = 0; i < SpellRegisterer.spells.size(); i++){
		    		if(SpellPattern.matchSpell(SpellRegisterer.spells.get(i), e.player)){
		    			SpellRegisterer.spells.get(i).runExecutorHand(e.player.posX, e.player.posY, e.player.posZ, e.player.worldObj, e.player);
		    			mana.setCast(false);
		    			mana.consumeMana(SpellRegisterer.spells.get(i).getCost());
		    			mana.getSpell().releaseSpell();
		    			PacketDispatcher.sendTo(new SyncManaPacket(mana.getMana()), (EntityPlayerMP)e.player);
		    		} else if(mana.getConstantManaDrain() > 0.0) { 
		    			PacketDispatcher.sendTo(new SyncManaPacket(mana.getMana()), (EntityPlayerMP)e.player);
		    		}
	    		}
    		}
    	}
    }
    /**
     * Copy data from dead player to the new player
     */
    @SubscribeEvent
    public void onPlayerClone(PlayerEvent.Clone event)
    {
        EntityPlayer player = event.entityPlayer;
        IMagicCapability mana = player.getCapability(MagicProvider.MAGIC_CAP, null);
        IMagicCapability oldMana = event.original.getCapability(MagicProvider.MAGIC_CAP, null);

        mana.setMana(oldMana.getMana());
    }
    @SubscribeEvent
    public void onPlayerExit(PlayerLoggedOutEvent e){
    	if(!e.player.worldObj.isRemote){
    		e.player.getCapability(MagicProvider.MAGIC_CAP, null).SetAllSpellFalse();
    	}
    }
}

