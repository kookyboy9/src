package com.kookyboy9.MagicMod;

import net.minecraft.init.Blocks;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.kookyboy9.MagicMod.Events.PlayerEvents;

@Mod(modid = MagicMod.MODID,name = MagicMod.MODNAME, version = MagicMod.VERSION)
public class MagicMod
{
		//logger. Use this instead of println statements, it is much better
		public static final Logger log = LogManager.getLogger("magicmod");
		//Mod id, Mod name, and Mod version. mod id should be all lowercase.
		public static final String MODID = "magicmod";
	    public static final String MODNAME = "Magic Mod";
	    public static final String VERSION = "1.0.0";
	    //A version of the mod.
	    @Instance
	    public static MagicMod instance = new MagicMod();
	    //This handles everything before the screen appears
	    @SidedProxy(clientSide="com.kookyboy9.MagicMod.ClientProxy", serverSide="com.kookyboy9.MagicMod.ServerProxy")
	    public static CommonProxy proxy;


	    @EventHandler
	    public void preInit(FMLPreInitializationEvent e) {
	        this.proxy.preInit(e);
	    }

	    @EventHandler
	    public void init(FMLInitializationEvent e) {
	        this.proxy.init(e);
	    }

	    @EventHandler
	    public void postInit(FMLPostInitializationEvent e) {
	        this.proxy.postInit(e);
	        MinecraftForge.EVENT_BUS.register(new PlayerEvents());
	        FMLCommonHandler.instance().bus().register(new KeyHandler());
	    }
}
