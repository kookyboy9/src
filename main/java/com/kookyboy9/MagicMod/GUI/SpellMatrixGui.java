package com.kookyboy9.MagicMod.GUI;

import java.io.IOException;
import java.util.ArrayList;

import org.lwjgl.opengl.GL11;

import com.kookyboy9.MagicMod.MagicMod;
import com.kookyboy9.MagicMod.GUI.Elements.LineElement;
import com.kookyboy9.MagicMod.GUI.Elements.TexturedButton;
import com.kookyboy9.MagicMod.Magic.Elements.ElementTypes;
import com.kookyboy9.MagicMod.Packets.PacketDispatcher;
import com.kookyboy9.MagicMod.Packets.Types.AddElementPackage;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.util.ResourceLocation;

public class SpellMatrixGui extends GuiScreen {
	private ElementTypes lastClicked = ElementTypes.EMPTY;
	private boolean hasClicked = false;
	private int clickX = -1;
	private int clickY = -1;
	private int mouseX;
	private int mouseY;
	private TexturedButton background;
	private ArrayList<LineElement> lines = new ArrayList<LineElement>();
	public SpellMatrixGui(){
		
	}
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		this.drawDefaultBackground();
		this.mouseX = mouseX;
		this.mouseY = mouseY;
	    //Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation(MagicMod.MODID, "textures/gui/pages/Intro4.png"));
	    //this.drawModalRectWithCustomSizedTexture(100, 2, 0, 0, 250, 241, 250, 241 );
	    //Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation(MagicMod.MODID, "textures/gui/casting_gui/LineA0.png"));
	    //this.drawModalRectWithCustomSizedTexture(275, 118, 0, 0, 44, 80, 44, 80);
	    //Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation(MagicMod.MODID, "textures/gui/casting_gui/LineO0.png"));
	    //this.drawModalRectWithCustomSizedTexture(275, 42, 0, 0, 43, 83, 43, 83);
	    //Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation(MagicMod.MODID, "textures/gui/casting_gui/LineA2.png"));
	    //this.drawModalRectWithCustomSizedTexture(275, 42, 0, 0, 6, 155, 6, 155);
		Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation(MagicMod.MODID, "textures/gui/casting_gui/LineA1.png"));
		//this.drawModalRectWithCustomSizedTexture(100, 10, 0, 0, 112, 8, 112, 8);
	    super.drawScreen(mouseX, mouseY, partialTicks);
	   

	}
	@Override
	public boolean doesGuiPauseGame() {
	    return false;
	}
	@Override
	public void initGui() {
		LineElement line;
		this.buttonList.add(this.background = new TexturedButton(1, 98, -8, 250, 241, new ResourceLocation(MagicMod.MODID, "textures/gui/pages/Intro4.png"), 1.0, 0, false));
		this.buttonList.add(line = new LineElement(0, 275, 118, 44, 80, new ResourceLocation(MagicMod.MODID, "textures/gui/casting_gui/LineA0.png") ));
		lines.add(line);
		this.buttonList.add(line = new LineElement(1, 169, 193, 112, 8, new ResourceLocation(MagicMod.MODID, "textures/gui/casting_gui/LineA1.png") ));
		lines.add(line);
		this.buttonList.add(line = new LineElement(2, 277, 39, 6, 159, new ResourceLocation(MagicMod.MODID, "textures/gui/casting_gui/LineA2.png") ));
		lines.add(line);
		this.buttonList.add(line = new LineElement(3, 168, 39, 114, 159, new ResourceLocation(MagicMod.MODID, "textures/gui/casting_gui/LineA3.png") ));
		lines.add(line);
		this.buttonList.add(line = new LineElement(4, 127, 115, 156, 86, new ResourceLocation(MagicMod.MODID, "textures/gui/casting_gui/LineA4.png") ));
		lines.add(line);
		this.buttonList.add(line = new LineElement(5, 127, 115, 47, 85, new ResourceLocation(MagicMod.MODID, "textures/gui/casting_gui/LineD0.png") ));
		lines.add(line);
		this.buttonList.add(line = new LineElement(6, 168, 39, 6, 162, new ResourceLocation(MagicMod.MODID, "textures/gui/casting_gui/LineE0.png") ));
		lines.add(line);
		this.buttonList.add(line = new LineElement(7, 128, 40, 46, 82, new ResourceLocation(MagicMod.MODID, "textures/gui/casting_gui/LineE1.png") ));
		lines.add(line);
		this.buttonList.add(line = new LineElement(8, 278, 40, 43, 83, new ResourceLocation(MagicMod.MODID, "textures/gui/casting_gui/LineO0.png") ));
		lines.add(line);
		this.buttonList.add(line = new LineElement(9, 167, 40, 155, 83, new ResourceLocation(MagicMod.MODID, "textures/gui/casting_gui/LineO1.png") ));
		lines.add(line);
		this.buttonList.add(line = new LineElement(10, 127, 116, 194, 6, new ResourceLocation(MagicMod.MODID, "textures/gui/casting_gui/LineO2.png") ));
		lines.add(line);
		this.buttonList.add(line = new LineElement(11, 168, 118, 153, 82, new ResourceLocation(MagicMod.MODID, "textures/gui/casting_gui/LineO3.png") ));
		lines.add(line);
		this.buttonList.add(line = new LineElement(12, 168, 40, 114, 6, new ResourceLocation(MagicMod.MODID, "textures/gui/casting_gui/LineW0.png") ));
		lines.add(line);
		this.buttonList.add(line = new LineElement(13, 127, 39, 155, 83, new ResourceLocation(MagicMod.MODID, "textures/gui/casting_gui/LineW1.png") ));
		lines.add(line);
		this.buttonList.add(line = new LineElement(14, 168, 39, 115, 162, new ResourceLocation(MagicMod.MODID, "textures/gui/casting_gui/LineW2.png") ));
		lines.add(line);
		for(int i = 0; i < lines.size(); i++){
			lines.get(i).visible = false;
		}
	}
	@Override
	public void updateScreen(){
		
	}
	@Override
	public void keyTyped(char key, int code) throws IOException{
		if(key == 'c'){
			PacketDispatcher.sendToServer(new AddElementPackage(7));
			this.mc.setIngameFocus();
		}
		if (code == 1){
            this.mc.displayGuiScreen((GuiScreen)null);

            if (this.mc.currentScreen == null)
            {
            	PacketDispatcher.sendToServer(new AddElementPackage(6));
                this.mc.setIngameFocus();
            }
        }
	}
	@Override
	protected void actionPerformed(GuiButton button) throws IOException {

	}
	@Override
	public void onGuiClosed(){
		
	}
	@Override
	public void mouseClicked(int mouseX, int mouseY, int clickedMouseButton){
		
		clickX = mouseX;
		clickY = mouseY;
		ElementTypes click = ElementTypes.EMPTY;
		if(Math.sqrt(Math.pow(278 - clickX, 2) + Math.pow(195 - clickY, 2)) <= 29){
			MagicMod.log.info("Air");
			click = ElementTypes.AIR;
			PacketDispatcher.sendToServer(new AddElementPackage(0));
		} else if(Math.sqrt(Math.pow(171 - clickX, 2) + Math.pow(197 - clickY, 2)) <= 29){
			MagicMod.log.info("Fire");
			click = ElementTypes.FIRE;
			PacketDispatcher.sendToServer(new AddElementPackage(1));
		} else if(Math.sqrt(Math.pow(132 - clickX, 2) + Math.pow(123 - clickY, 2)) <= 29){
			MagicMod.log.info("Dark");
			click = ElementTypes.DARK;
			PacketDispatcher.sendToServer(new AddElementPackage(2));
		} else if(Math.sqrt(Math.pow(172 - clickX, 2) + Math.pow(51 - clickY, 2)) <= 29){
			MagicMod.log.info("Earth");
			click = ElementTypes.EARTH;
			PacketDispatcher.sendToServer(new AddElementPackage(3));
		} else if(Math.sqrt(Math.pow(279 - clickX, 2) + Math.pow(51 - clickY, 2)) <= 29){
			MagicMod.log.info("Water");
			click = ElementTypes.WATER;
			PacketDispatcher.sendToServer(new AddElementPackage(4));
		} else if(Math.sqrt(Math.pow(317 - clickX, 2) + Math.pow(124 - clickY, 2)) <= 29){
			MagicMod.log.info("Light");
			click = ElementTypes.LIGHT;
			PacketDispatcher.sendToServer(new AddElementPackage(5));
		}
		MagicMod.log.info("Click Detected at X: " + clickX + " Y: " + clickY);
		if(!hasClicked) {
			hasClicked = true;
		} else {
			hasClicked = false;
		}
		switch(lastClicked)
		{
		case AIR: switch(click){
			case WATER: lines.get(2).visible = true; break;
			case AIR: break;
			case LIGHT: lines.get(0).visible = true; break;
			case DARK: lines.get(4).visible = true; break;
			case EARTH: lines.get(3).visible = true; break;
			case FIRE: lines.get(1).visible = true; break;
			default: break;
			}  break;
		case WATER: switch(click){
			case WATER: break;
			case AIR: lines.get(2).visible = true; break;
			case LIGHT: lines.get(8).visible = true; break;
			case DARK: lines.get(13).visible = true; break;
			case EARTH: lines.get(12).visible = true; break;
			case FIRE: lines.get(14).visible = true; break;
			default: break;
			}  break;
		case FIRE: switch(click){
			case WATER: lines.get(14).visible = true; break;
			case AIR: lines.get(1).visible = true; break;
			case LIGHT: lines.get(11).visible = true; break;
			case DARK: lines.get(5).visible = true; break;
			case EARTH: lines.get(6).visible = true; break;
			case FIRE: break;
			default: break;
			}  break;
		case EARTH: switch(click){
			case WATER: lines.get(12).visible = true; break;
			case AIR: lines.get(3).visible = true; break;
			case LIGHT: lines.get(9).visible = true; break;
			case DARK: lines.get(7).visible = true; break;
			case EARTH:  break;
			case FIRE: lines.get(6).visible = true; break;
			default: break;
			}  break;
		case LIGHT: switch(click){
			case WATER: lines.get(8).visible = true; break;
			case AIR: lines.get(0).visible = true; break;
			case LIGHT: break;
			case DARK: lines.get(10).visible = true; break;
			case EARTH: lines.get(9).visible = true;  break;
			case FIRE: lines.get(11).visible = true; break;
			default: break;
			} break;
		case DARK: switch(click){
			case WATER: lines.get(13).visible = true; break;
			case AIR: lines.get(4).visible = true; break;
			case LIGHT: lines.get(10).visible = true; break;
			case DARK: break;
			case EARTH: lines.get(7).visible = true;  break;
			case FIRE: lines.get(5).visible = true; break;
			default: break;
			} break;
		default: break;
		}
		if(click != ElementTypes.EMPTY){
			lastClicked = click;
		}
	}
}
