package com.kookyboy9.MagicMod.GUI.Elements;
import org.lwjgl.opengl.GL11;

import com.kookyboy9.MagicMod.MagicMod;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.ResourceLocation;

public class LineElement extends GuiButton{
	private ResourceLocation texture;
	int x;
	int y;
	int width; 
	int height; 
	public LineElement(int id, int x, int y, int width, int height, ResourceLocation texture) {
		super(id, x, y, width, height, "");
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.texture = texture;
	}
	@Override
    public void drawButton(Minecraft mc, int mouseX, int mouseY)
    {
        if (this.visible)
        {
            FontRenderer fontrenderer = mc.fontRendererObj;
            Minecraft.getMinecraft().getTextureManager().bindTexture(texture);
            GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
            this.hovered = mouseX >= this.xPosition  * .1 && mouseY >= this.yPosition * .1 && mouseX < (this.xPosition + this.width) * .1 && mouseY < (this.yPosition + this.height) * .1;
            int i = 0;
            GlStateManager.enableBlend();
            GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
            GlStateManager.blendFunc(770, 771);
            this.drawModalRectWithCustomSizedTexture(this.xPosition, this.yPosition, 0, 0, this.width, this.height, this.width, this.height);
            this.mouseDragged(mc, mouseX, mouseY);
            int j = 0xCCCCCC;
            
        }
    }
	@Override
    /**
     * Returns true if the mouse has been pressed on this control. Equivalent of MouseListener.mousePressed(MouseEvent
     * e).
     */
    public boolean mousePressed(Minecraft mc, int mouseX, int mouseY)
    {
        return this.enabled && this.visible && this.hovered;
    }
}