package com.kookyboy9.MagicMod.GUI.Elements;

import org.lwjgl.opengl.GL11;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.EnumChatFormatting;

public class LinkedText extends GuiButton{
	private int chapter = 0;
	public LinkedText(int id, int x, int y, String s, int chapter) {
		super(id, x, y, (int)(s.length() * 5.5) + 2, 12, s);
		this.chapter = chapter;
	}
	@Override
    public void drawButton(Minecraft mc, int mouseX, int mouseY)
    {
        if (this.visible)
        {
        	
        	GL11.glScaled(.65, .65, .65);
            FontRenderer fontrenderer = mc.fontRendererObj;
            GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
            this.hovered = mouseX >= this.xPosition *.65 && mouseY >= this.yPosition * .65 && mouseX < (this.xPosition + this.width) *.65 && mouseY < (this.yPosition + this.height) * .65;
            int i = this.getHoverState(this.hovered);
            GlStateManager.enableBlend();
            GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
            GlStateManager.blendFunc(770, 771);
            //this.drawTexturedModalRect(this.xPosition, this.yPosition, 0, 46 + i * 20, this.width / 2, this.height);
            //this.drawTexturedModalRect(this.xPosition + this.width / 2, this.yPosition, 200 - this.width / 2, 46 + i * 20, this.width / 2, this.height);
            this.mouseDragged(mc, mouseX, mouseY);
            int j = 0xCCCCCC;

            if (packedFGColour != 0)
            {
                j = packedFGColour;
            }
            else
            if (!this.enabled)
            {
                j = 0xDDDDDD;
            }
            else if (this.hovered)
            {
                j = 0xEEEEEE;
            }
            this.drawCenteredString(fontrenderer, EnumChatFormatting.UNDERLINE + this.displayString, this.xPosition + this.width / 2, this.yPosition + (this.height - 8) / 2, j);
            GL11.glScaled(1/.65, 1/.65, 1/.65);
        }
    }
	@Override
    /**
     * Returns true if the mouse has been pressed on this control. Equivalent of MouseListener.mousePressed(MouseEvent
     * e).
     */
    public boolean mousePressed(Minecraft mc, int mouseX, int mouseY)
    {
        return this.enabled && this.visible && this.hovered;
    }

	public int getChapter(){
		return this.chapter;
	}
}