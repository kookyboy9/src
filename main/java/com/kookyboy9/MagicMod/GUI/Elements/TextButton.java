package com.kookyboy9.MagicMod.GUI.Elements;

import java.util.ArrayList;

import org.lwjgl.opengl.GL11;

import com.kookyboy9.MagicMod.MagicMod;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.EnumChatFormatting;

public class TextButton extends GuiButton{
	static boolean scaled = false;
	private String s = "";
	private ArrayList<String> as = new ArrayList<String>();
	public TextButton(int id, int x, int y, String c) {
		super(id, x, y, (int)(c.length() * 5.5) + 2, 12, c);
		int added = 0;
    	if(s == "" && this.displayString != ""){
        	for(int i= 0; i < this.displayString.length(); i++ ){
        		s += this.displayString.charAt(i);
        		added += 1;
        		if(added % 29 == 28){
        			added = 0;
        			int l = s.length() - 1;
        			int counter = 0;
        			while(s.charAt(l) != ' ') {
        				l--;
        				counter++;
        				s = s.substring(0, s.length() - 1);
        			}
        			as.add(s);
        			s = "";
        			while(counter >= 0){
        				s += this.displayString.charAt(i - counter);
        				counter--;
        				added+= 1;
        			}
        		}
        	}
    	}
	}
	@Override
    public void drawButton(Minecraft mc, int mouseX, int mouseY)
    {
        if (this.visible)
        {
            GL11.glScaled(.65, .65, .65);
        	scaled = true;
            FontRenderer fontrenderer = mc.fontRendererObj;
            GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
            this.hovered = mouseX >= this.xPosition * .65 && mouseY >= this.yPosition && mouseX < this.xPosition + this.width && mouseY < this.yPosition + this.height;
            int i = this.getHoverState(this.hovered);
            GlStateManager.enableBlend();
            GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
            GlStateManager.blendFunc(770, 771);
            //this.drawTexturedModalRect(this.xPosition, this.yPosition, 0, 46 + i * 20, this.width / 2, this.height);
            //this.drawTexturedModalRect(this.xPosition + this.width / 2, this.yPosition, 200 - this.width / 2, 46 + i * 20, this.width / 2, this.height);
            //this.mouseDragged(mc, mouseX, mouseY);
            int j = 0x666666;

            /*if (packedFGColour != 0)
            {
                j = packedFGColour;
            }
            else
            if (!this.enabled)
            {
                j = 10526880;
            }
            else if (this.hovered)
            {
                j = 16777120;
            }
			*/
            int f = 0;
            if(as.isEmpty()){} else {
            	for(int m = 0; m < as.size(); m++){
            		fontrenderer.drawString( as.get(m), this.xPosition, this.yPosition + (m * 15), j);
            		f = m + 1;
            	}
            }
            fontrenderer.drawString(s, this.xPosition, this.yPosition + (f * 15), j);



            
            GL11.glScaled(1/.65, 1/.65, 1/.65);
        }
    }
}
