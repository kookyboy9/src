package com.kookyboy9.MagicMod.GUI.Elements;

import org.lwjgl.opengl.GL11;

import com.kookyboy9.MagicMod.MagicMod;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.ResourceLocation;

public class TexturedButton extends GuiButton{
	private ResourceLocation texture;
	private double scale = 1.0;
	private boolean frames = true;
	int x;
	int y;
	int offset;
	public TexturedButton(int id, int x, int y, int width, int height, ResourceLocation rl, double scale, int offset, boolean frames) {
		super(id, x, y,width, height, "");
		texture = rl;
		this.x= x;
		this.y = y;
		this.scale  = scale;
		this.frames = frames;
		this.offset = offset;
	}
	@Override
    public void drawButton(Minecraft mc, int mouseX, int mouseY)
    {
        if (this.visible)
        {
        	GL11.glScaled(scale, scale, scale);
        	GlStateManager.disableRescaleNormal();
            RenderHelper.disableStandardItemLighting();
            GlStateManager.disableLighting();
            GlStateManager.disableDepth();
            FontRenderer fontrenderer = mc.fontRendererObj;
            Minecraft.getMinecraft().getTextureManager().bindTexture(this.texture);
            GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
            this.hovered = mouseX >= this.xPosition  * .1 && mouseY >= this.yPosition * .1 && mouseX < (this.xPosition + this.width) * .1 && mouseY < (this.yPosition + this.height) * .1;
            int i = this.getHoverState(this.hovered);
            if(this.frames == false) {
            	i = 0;
            }
            GlStateManager.enableBlend();
            GlStateManager.tryBlendFuncSeparate(770, 771, 1, 0);
            GlStateManager.blendFunc(770, 771);
            this.drawTexturedModalRect(this.x, this.y, 0, (i * (this.height) + offset), this.width, this.height);
            this.mouseDragged(mc, mouseX, mouseY);
            int j = 0xCCCCCC;
            GL11.glScaled(1/scale, 1/scale, 1/scale);
            GlStateManager.enableLighting();
            GlStateManager.enableDepth();
            RenderHelper.enableStandardItemLighting();
            GlStateManager.enableRescaleNormal();
        }
    }
	@Override
    /**
     * Returns true if the mouse has been pressed on this control. Equivalent of MouseListener.mousePressed(MouseEvent
     * e).
     */
    public boolean mousePressed(Minecraft mc, int mouseX, int mouseY)
    {
        return this.enabled && this.visible && this.hovered;
    }
}