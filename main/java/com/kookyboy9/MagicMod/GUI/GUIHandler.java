package com.kookyboy9.MagicMod.GUI;

import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

public class GUIHandler implements IGuiHandler {
	public static final int MOD_GUIDE_GUI = 0;
	public static final int MOD_MAGIC_GUI = 1;

    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        if (ID == MOD_GUIDE_GUI)
           // return new ContainerModTileEntity();
        	return null;

        return null;
    }

    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        if (ID == MOD_GUIDE_GUI){
        	return new GuideGUI();
        }
        if (ID == MOD_MAGIC_GUI) {
        	return new SpellMatrixGui();
        }
        return null;
    }
}