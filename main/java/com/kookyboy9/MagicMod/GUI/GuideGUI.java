package com.kookyboy9.MagicMod.GUI;

import java.io.IOException;
import java.util.ArrayList;

import org.lwjgl.opengl.GL11;

import com.kookyboy9.MagicMod.MagicMod;
import com.kookyboy9.MagicMod.GUI.Elements.LinkedText;
import com.kookyboy9.MagicMod.GUI.Elements.TextButton;
import com.kookyboy9.MagicMod.GUI.Elements.TexturedButton;
import com.kookyboy9.MagicMod.GUI.GuidePages.Chapter;
import com.kookyboy9.MagicMod.GUI.GuidePages.Page;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.EnumChatFormatting;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class GuideGUI extends GuiScreen {
	private ArrayList<Chapter> contents = new ArrayList<Chapter>();
	private int Chapter = -1;
	private int index = 0;
	private TextButton title;
	private LinkedText Intro;
	private boolean update = false;
	private TextButton content;
	private TextButton content2;
	private TexturedButton contentp;
	private TexturedButton content2p;
	private int page = 0;
	private GuiButton backButton;
	private GuiButton forwardButton;
	private int time = 20;
	private boolean timer = false;
	
	public GuideGUI(){
		//initialize pages and chapters here.
		Page Intro1 = new Page(" ");
		Chapter Intro = new Chapter("Basics of Magic", Intro1);
		Page Intro2 = new Page("Basics of Magic.");
		Page Intro3 = new Page(
				"Magic can be harnessed in one of two ways."
				+ " The first method is to draw forth and"
				+ " use your personal magical array."
				+ " Using this method, one simply draws"
				+ " a design connecting the elements in"
				+ " an unique order to create spells.");
		Page Intro4 = new Page(
				new ResourceLocation(MagicMod.MODID, "textures/gui/pages/Intro4.png"));
		Page Intro5 = new Page(
				"The second method is to create the matrix "
				+ "in the world, then use the spell with "
				+ "the built matrix. These spells tend to "
				+ "effect a larger area.");
		Intro.addPage(Intro2);
		Intro.addPage(Intro3);
		Intro.addPage(Intro4);
		Intro.addPage(Intro5);
		contents.add(Intro);
	}
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
	    this.drawDefaultBackground();
	    GlStateManager.enableBlend();
	    Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation(MagicMod.MODID, "textures/gui/guide_gui_background.png"));
	    this.drawModalRectWithCustomSizedTexture(this.width - (int)(this.width * (.8)), this.height - (int)(this.height * (.99)), 0, 0, 250, 241, 250, 241 );
	    super.drawScreen(mouseX, mouseY, partialTicks);
	}
	@Override
	public boolean doesGuiPauseGame() {
	    return false;
	}
	@Override
	public void initGui() {
		
		this.buttonList.add(this.title = new TextButton(0, (int)(this.width * (.36)), this.height/2 - (int)(this.height * (.4)), EnumChatFormatting.BOLD + "Table of Contents"));
		this.buttonList.add(this.Intro = new LinkedText(1, (int)(this.width * (.38)), this.height/2 - (int)(this.height * (.31)), "Introduction to Magic", 0));
		this.buttonList.add(this.content = new TextButton(2, (int)(this.width * (.375)), this.height/2 - (int)(this.height * (.31)), ""));
		this.buttonList.add(this.content2 = new TextButton(3, (int)(this.width * (.75)), this.height/2 - (int)(this.height * (.31)), ""));
		this.buttonList.add(this.contentp = new TexturedButton(2, (int)(this.width * (.375)), this.height/2 - (int)(this.height * (.31)), 200, 200, new ResourceLocation(MagicMod.MODID, "textures/gui/pages/Intro4.png"), .05, -2, false));
		this.buttonList.add(this.content2p  = new TexturedButton(2, (int)(this.width * (.375)), this.height/2 - (int)(this.height * (.31)), 200, 200, new ResourceLocation(MagicMod.MODID, "textures/gui/pages/Intro4.png"), .05, -2, false));
		this.buttonList.add(this.backButton = new TexturedButton(4, (int)(this.width * 2.4), (int)(this.height*(5.85)), 260, 130, new ResourceLocation(MagicMod.MODID, "textures/gui/back_arrow.png"), .1, -7, true));
		this.buttonList.add(this.forwardButton = new TexturedButton(4, (int)(this.width * 7), (int)(this.height*(5.85)), 260, 130, new ResourceLocation(MagicMod.MODID, "textures/gui/forward_arrow.png"), .1, -7, true));
		this.content.visible = false;
		this.content2.visible = false;
		this.content2p.visible = false;
		this.contentp.visible = false;
		this.backButton.visible = false;
		this.forwardButton.visible = false;
	}
	@Override
	public void updateScreen(){
		if(timer) {
			this.time++;
		}
		if(time >= 20 && timer){
			timer = false;
		}
		if(this.update) {
			if(this.Chapter >= 0){
				
				this.Intro.enabled = false;
				this.Intro.visible = false;
				
				if(this.contents.get(this.Chapter).getPage(this.page).getContents() != null){
					this.contentp.visible = false;
					this.buttonList.remove(this.content);
					this.buttonList.add(this.content = new TextButton(2, (int)(this.width * (.375)), this.height/2 - (int)(this.height * (.31)), this.contents.get(this.Chapter).getPage(this.page).getContents()));
				} else if(this.contents.get(this.Chapter).getPage(this.page).getPicture() != null){
					this.content.visible = false;
					this.buttonList.remove(this.contentp);
					this.buttonList.add(this.contentp = new TexturedButton(2, (int)(this.width * (.375)), this.height/2 - (int)(this.height * (.31)), 250, 250, this.contents.get(this.Chapter).getPage(this.page).getPicture(), .4, 0, false));
				}else {
					this.content.visible = false;
					this.contentp.visible = false;
				}
				if(this.contents.get(Chapter).getPage(this.page + 1) != null){
					if(this.contents.get(this.Chapter).getPage(this.page + 1).getContents() != null) {
						this.content2p.visible = false;
						this.buttonList.remove(this.content2);
						this.buttonList.add(this.content2 = new TextButton(2, (int)(this.width * (.77)), this.height/2 - (int)(this.height * (.31)), this.contents.get(this.Chapter).getPage(this.page + 1).getContents()));
					} else if(this.contents.get(this.Chapter).getPage(this.page + 1).getPicture() != null) {
						this.content2.visible = false;
						this.buttonList.remove(this.content2p);
						this.buttonList.add(this.content2p = new TexturedButton(2, (int)(this.width * (1.25)), this.height/2 - (int)(this.height * (.21)), 250, 250, this.contents.get(this.Chapter).getPage(this.page + 1).getPicture(), .4, 0, false));
					}else {
						this.content2.visible = false;
						this.content2p.visible = false;
					}
				} else {
					this.content2.visible = false;
					this.content2p.visible = false;
				}
				if(this.page == 0){
					this.buttonList.remove(this.title);
					this.buttonList.add(this.title = new TextButton(0, (int)(this.width * (.36)), this.height/2 - (int)(this.height * (.4)), EnumChatFormatting.BOLD + this.contents.get(this.Chapter).title));
					this.backButton.enabled = false;
					this.backButton.visible = false;
				} else {
					this.backButton.enabled = true;
					this.backButton.visible = true;
				}
				if(this.page == this.contents.get(this.Chapter).pages.size() - 1 || this.page + 1 == this.contents.get(this.Chapter).pages.size() - 1 ){
					this.forwardButton.enabled = false;
					this.forwardButton.visible = false;
				} else {
					this.forwardButton.enabled = true;
					this.forwardButton.visible = true;
				}
			}
		}
	}
	@Override
	protected void actionPerformed(GuiButton button) throws IOException {
		for(int i = 0; i < this.buttonList.size(); i++){
			if(this.buttonList.get(i) instanceof LinkedText) {
				if(button == this.buttonList.get(i)){
					this.update = true;
					this.page = 0;
					this.Chapter = ((LinkedText) (this.buttonList.get(i))).getChapter();
				}
				
			}
			else if(button == this.backButton && this.time >= 20){
				this.page -= 2;
				this.update = true;
				this.timer = true;
				this.time = 0;
			}
			else if(button == this.forwardButton && this.time >= 20){
				this.page += 2;
				this.update = true;
				this.timer = true;
				this.time = 0;
			}
		}
	}
}
