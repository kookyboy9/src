package com.kookyboy9.MagicMod.GUI.GuidePages;

import java.util.ArrayList;

import net.minecraft.util.ResourceLocation;

public class Chapter {
	public ArrayList<Page> pages = new ArrayList<Page>(); 
	public int currentPage = 0;
	public String title;
	
	public Chapter(String s, ArrayList<Page> pages) {
		this.pages = pages;
		title = s;
	}
	public Chapter(String s, Page page){
		pages.add(page);
		title = s;
	}
	public Page getPage(int i) {
		try{
			return  pages.get(i);
		} catch(IndexOutOfBoundsException e) {}
		return null;
	}
	public void addPage(Page page){
		pages.add(page);
	}
}
