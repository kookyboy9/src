package com.kookyboy9.MagicMod.GUI.GuidePages;

import net.minecraft.util.ResourceLocation;

public class Page {
	private String contents;
	private ResourceLocation picture;
	
	public Page(String s) {
		contents = s;
	}
	public Page(ResourceLocation loc) {
		picture = loc;
	}
	public void setContents(String s) {
		contents = s;
	}
	public void setPicture(ResourceLocation loc) {
		picture = loc;
	}
	public String getContents() {
		return contents;
	}
	public ResourceLocation getPicture() {
		return picture;	
	}
}
