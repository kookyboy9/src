package com.kookyboy9.MagicMod.MagicCapability;

import java.util.ArrayList;
import java.util.Iterator;

import com.kookyboy9.MagicMod.MagicMod;
import com.kookyboy9.MagicMod.Magic.Elements;
import com.kookyboy9.MagicMod.Magic.Elements.ElementTypes;
import com.kookyboy9.MagicMod.Magic.Spell;
import com.kookyboy9.MagicMod.Magic.SpellPattern;
import com.kookyboy9.MagicMod.Magic.SpellRegisterer;

import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;

public class MagicCapability implements IMagicCapability
{
	private Spell spell = new Spell();
    private float mana = 100.0F;
	private boolean cast = false;
	private float MaxMana = 100.0f;
	private float constantDrain;
	private boolean MaxManaIncreasef = true;
	private boolean magicPunch = false;
	@Override
	public float GetMaxMana(){
		return this.MaxMana;
	}
	@Override
	public void SetMaxMana(float mana){
		this.MaxMana = mana;
	}
    @Override
    public void consumeMana(float points)
    {
        this.mana -= points;

        if (this.mana < 0.0F) this.mana = 0.0F;
    }

    @Override
    public void fillMana(float points)
    {
        this.mana += points;
    }

    @Override
    public void setMana(float points)
    {
        this.mana = points;
    }

    @Override
    public float getMana()
    {
        return this.mana;
    }

	@Override
	public IMagicCapability getCapability() {
		return this;
	}
	@Override
	public void AddElementToSpell(int i){
		switch(i){
			case 0: spell.addElement(ElementTypes.AIR); break;
			case 1: spell.addElement(ElementTypes.FIRE); break;
			case 2: spell.addElement(ElementTypes.DARK); break;
			case 3: spell.addElement(ElementTypes.EARTH); break;
			case 4: spell.addElement(ElementTypes.WATER); break;
			case 5: spell.addElement(ElementTypes.LIGHT); break;
			case 6: spell.releaseSpell(); break;
			case 7: this.cast  = true; break;
		}
		Iterator<ElementTypes> it = spell.getSpell();
		while(it.hasNext()){
			MagicMod.log.info(it.next());
		}
	}

	@Override
	public boolean getCast() {
		return this.cast;
		
	}
	@Override
	public void setCast(boolean cast) {
		this.cast = cast;
		
	}

	@Override
	public Spell getSpell() {
		return spell;
	}
	@Override
	public float getConstantManaDrain() {
		return constantDrain;
	}

	@Override
	public void removeConstantManaDrain(float f) {
		constantDrain -= f;
	}
	@Override
	public void addConstantManaDrain(float f) {
		constantDrain += f;
	}
	@Override
	public boolean shouldIncreaseMaxMana(){
		return MaxManaIncreasef;
	}
	@Override
	public void toggleIncreaseMaxMana(){
		MaxManaIncreasef = !MaxManaIncreasef;
	}
	@Override
	public void ReadFromNBT(NBTTagCompound data) {
		this.mana = data.getFloat("Mana");
		this.MaxMana = data.getFloat("MaxMana");
		this.constantDrain = data.getFloat("ConstantDrain");
		
	}
	@Override
	public NBTTagCompound WriteToNBT() {
		NBTTagCompound data = new NBTTagCompound();
		data.setFloat("Mana", this.mana);
		data.setFloat("MaxMana", this.MaxMana);
		data.setFloat("ConstantDrain", this.constantDrain);
		return data;
	}

	/**********************************************
	/* Only intended for use when all active spells
	/* should be canceled.
	/**********************************************/
	@Override
	public void SetAllSpellFalse(){
		
	}
	@Override
	public boolean getMagicPunch() {
		return this.magicPunch;
	}
	@Override
	public void setMagicPunch(boolean b) {
		this.magicPunch = b;
		
	}
}
