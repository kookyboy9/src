package com.kookyboy9.MagicMod.MagicCapability;

import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagFloat;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;

public class MagicStorage implements Capability.IStorage<IMagicCapability> {
    @Override
    public NBTBase writeNBT(Capability<IMagicCapability> capability, IMagicCapability instance, EnumFacing side)
    {
        return new NBTTagFloat(instance.getMana());
    }

    @Override
    public void readNBT(Capability<IMagicCapability> capability, IMagicCapability instance, EnumFacing side, NBTBase nbt)
    {
        instance.setMana(((NBTTagFloat) nbt).getFloat());
    }
}