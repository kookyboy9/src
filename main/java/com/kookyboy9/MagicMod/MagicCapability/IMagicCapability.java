package com.kookyboy9.MagicMod.MagicCapability;

import com.kookyboy9.MagicMod.Magic.Spell;

import net.minecraft.nbt.NBTTagCompound;

public interface IMagicCapability {
	boolean getCast();
	void setCast(boolean cast);
	public IMagicCapability getCapability();
	float getMana();
	void consumeMana(float points);
	void fillMana(float points);
	void setMana(float points);
	void AddElementToSpell(int i);
	Spell getSpell();
	float GetMaxMana();
	void SetMaxMana(float mana);
	float getConstantManaDrain();
	void addConstantManaDrain(float f);
	void removeConstantManaDrain(float f);
	void toggleIncreaseMaxMana();
	boolean shouldIncreaseMaxMana();
	void ReadFromNBT(NBTTagCompound id);
	NBTTagCompound WriteToNBT();
	void SetAllSpellFalse();
	boolean getMagicPunch();
	void setMagicPunch(boolean b);
}
