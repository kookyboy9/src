package com.kookyboy9.MagicMod.MagicCapability;

import net.minecraft.nbt.NBTBase;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityInject;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;

/**
 * Mana provider
 *
 * This class is responsible for providing a capability. Other modders may 
 * attach their own provider with implementation that returns another 
 * implementation of IMagicCapability to the target's (Entity, TE, ItemStack, etc.) disposal.
 */
public class MagicProvider implements ICapabilitySerializable<NBTBase>
{
    @CapabilityInject(IMagicCapability.class)
    public static final Capability<IMagicCapability> MAGIC_CAP = null;

    private IMagicCapability instance = MAGIC_CAP.getDefaultInstance();

    @Override
    public boolean hasCapability(Capability<?> capability, EnumFacing facing)
    {
        return capability == MAGIC_CAP;
    }

    @Override
    public <T> T getCapability(Capability<T> capability, EnumFacing facing)
    {
        return (T) (capability == MAGIC_CAP ? this.instance : null);
    }

    @Override
    public NBTBase serializeNBT()
    {
        return MAGIC_CAP.getStorage().writeNBT(MAGIC_CAP, this.instance, null);
    }

    @Override
    public void deserializeNBT(NBTBase nbt)
    {
        MAGIC_CAP.getStorage().readNBT(MAGIC_CAP, this.instance, null, nbt);
    }
}